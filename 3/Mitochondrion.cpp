#include "Mitochondrion.h"

/*
 * This function initializes the Mitochondrion class.
 * Return value: None
*/
void Mitochondrion::init()
{
    // Set initial values for properties
    this->_glocuse_level = 0;
    this->_has_glocuse_receptor = false;
}

/*
 * This function inserts a protien as the glucose receptor for the mitochondrion
 * Param protein: The reference to the protein
 * Return value: None
*/
void Mitochondrion::insert_glucose_receptor(const Protein & protein)
{
    int i = 0;
    bool validReceptor = true;
    
    // Create a list of the valid order of the amino acids in the glucose receptor
    AminoAcid valid_glocus_receptor[] = { ALANINE, LEUCINE, GLYCINE, HISTIDINE, LEUCINE, PHENYLALANINE, AMINO_CHAIN_END };

    AminoAcidNode *node = protein.get_first();

    // Go through the amino acids in the protein and check they are in the same order
    for (int i = 0; i < VALID_GLUCOSE_RECEPTOR_SIZE && validReceptor; i++)
    {
	// If the current amino acid isn't the same as in the valid glucose receptor, set valid to false and break
	if (node->get_data() != valid_glocus_receptor[i])
	{
	    validReceptor = false;
	}
	else {
	    // Procced to the next node in the list
	    node = node->get_next();
	}
    }

    // If the receptor given is a valid one, set has glucous receptor as true
    if (validReceptor)
    {
	this->_has_glocuse_receptor = true;
    }
}

/*
 * This function sets the glucose level of the mitochondrion
 * Param glocuse_units: The new value of the glucose level
 * Return value: None
*/
void Mitochondrion::set_glucose(const unsigned int glocuse_units)
{
    // Set the new glocuse untis
    this->_glocuse_level = glocuse_units;
}

/*
 * This function checks if the mitochondrion can produce ATP.
 * Return value: True if the mitochondrion can produce ATP or False otherwise
*/
bool Mitochondrion::produceATP() const
{
    bool canProduceATP = false;

    // If we have a glocuse receptor and at least 50 glocuse level, return true
    if (this->_has_glocuse_receptor && this->_glocuse_level >= 50)
    {
	canProduceATP = true;
    }

    return canProduceATP;
}


