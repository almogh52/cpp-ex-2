#pragma once

#include <string>

#include "Nucleus.h"
#include "Ribosome.h"
#include "Mitochondrion.h"

class Cell
{
public:
    void init(const std::string dna_sequence, const Gene glucose_receptor_gene);
    bool get_ATP();
    
    // Getters (used in the bonus)
    Nucleus getNucleus() const;
    Gene getGlocusReceptorGene() const;

private:
    Nucleus _nucleus;
    Ribosome _ribosome;
    Mitochondrion _mitochondrion;
    Gene _glocus_receptor_gene;
    unsigned int _atp_units;

};

