#include "Cell.h"

using std::cerr;
using std::endl;

/*
 * This function initializes the Cell class.
 * Param dna_sequence: The DNA sequence
 * Param glucose_receptor_gene: The glucose receptor gene
 * Return value: None
*/
void Cell::init(const std::string dna_sequence, const Gene glucose_receptor_gene)
{
    // Init the nucleus of the cell using the given dna seq
    this->_nucleus.init(dna_sequence);

    // Set the receptor gene
    this->_glocus_receptor_gene = glucose_receptor_gene;

    // Init the Mitochondrion
    this->_mitochondrion.init();
}

/*
 * This function loads the energy in the cell and loads up the ATP.
 * Return value: True if the ATP has been produced or False otherwise
*/
bool Cell::get_ATP()
{
    bool produced = false;
    Protein *protein = nullptr;
    std::string rna_transcript = "";

    // Get the RNA transcript for the glocus receptor gene
    rna_transcript = this->_nucleus.get_RNA_transcript(this->_glocus_receptor_gene);

    // Try to create a protein from the RNA transcript
    protein = this->_ribosome.create_protein(rna_transcript);

    // If the protein couldn't be generated, send an error and exit
    if (!protein)
    {
	cerr << "Unable to generate protein!" << endl;
	_exit(1);
    }
    else 
    {
	// Insert the protein as a glucose receptor in the mitochondrion
	this->_mitochondrion.insert_glucose_receptor(*protein);

	// Set glucose level at 50 in order to start producing ATP
	this->_mitochondrion.set_glucose(50);

	// Try to produce ATP, if it was produced, set produced to true and set atp units to 100
	if (this->_mitochondrion.produceATP())
	{
	    produced = true;

	    // Set cell as full of energy
	    this->_atp_units = 100;
	}
    }

    return produced;
}

// Getters

/*
 * This function gets the nucleus field in the Cell class
 * Return value: The cell's nucleus
*/
Nucleus Cell::getNucleus() const
{
    return this->_nucleus;
}

/*
 * This function gets the glocus receptor gene field in the Cell class
 * Return value: The cell's glocus receptor gene
*/
Gene Cell::getGlocusReceptorGene() const
{
    return this->_glocus_receptor_gene;
}


