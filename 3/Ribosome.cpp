#include "Ribosome.h"

/*
 * This function creates protein according to a given RNA transcript
 * Param RNA_transcript: The RNA transcript to be converted into a protein
 * Return value: The pointer to the created protein if the protein was successfully created or nullptr if failed to be created
*/
Protein* Ribosome::create_protein(std::string &RNA_transcript) const
{
    int i = 0;

    std::string aminoAcidCoding = "";
    AminoAcid aminoAcid = UNKNOWN;

    // Create a protein and init it
    Protein *protein = new Protein;
    protein->init();

    // Go through the amino acids coding in the RNA transcript (Every 3 characters in the transcript is a amino acid coding)
    for (i = 0; i < RNA_transcript.length() && protein != nullptr; i += AMINO_ACID_SIZE)
    {
	// Slice from the RNA transcript the current amino acid coding (i - i + 3)
	aminoAcidCoding = RNA_transcript.substr(i, AMINO_ACID_SIZE);

	// Try to ged the amino acid for the amino acid coding
	aminoAcid = get_amino_acid(aminoAcidCoding);

	// If the amino acid wasn't found, clear the protein, delete it and return null
	if (aminoAcid == UNKNOWN)
	{
	    // Clear the protein list
	    protein->clear();

	    // Delete the protein object
	    delete protein;

	    // Set protein as nullptr to return it
	    protein = nullptr;
	}
	else {
	    // Add the amino acid to the protein
	    protein->add(aminoAcid);
	}
    }

    return protein;
}