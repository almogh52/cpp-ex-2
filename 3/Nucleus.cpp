#include "Nucleus.h"

using std::cerr;
using std::endl;

/*
 * This function initializes the Gene class.
 * Param start: The start of the gene in the DNA strand
 * Param end: The end of the gene in the DNA strand
 * Param on_complementary_dna_strand: Is the gene on the complementary dan strand or not
 * Return value: None
*/
void Gene::init(const unsigned int start, const unsigned int end, const bool on_complementary_dna_strand)
{
    // Set private field using init function
    this->_start = start;
    this->_end = end;
    this->_on_complementary_dna_strand = on_complementary_dna_strand;
}

// Getters

/*
 * This function gets the start of the gene in the DNA strand
 * Return value: The start of the gene in the DNA strand
*/
unsigned int Gene::get_start() const
{
    return this->_start;
}

/*
 * This function gets the end of the gene in the DNA strand
 * Return value: The end of the gene in the DNA strand
*/
unsigned int Gene::get_end() const
{
    return this->_end;
}

/*
 * This function gets if the gene is on the complementary DNA strand
 * Return value: True if the gene is on the complementary DNA strand or False otherwise
*/
bool Gene::is_on_complementary_dna_strand() const
{
    return this->_on_complementary_dna_strand;
}

// Setters

/*
 * This function sets the start of the gene in the DNA strand.
 * Param start: The start of the gene in the DNA strand
 * Return value: None
*/
void Gene::set_start(const unsigned int start)
{
    this->_start = start;
}

/*
 * This function sets the end of the gene in the DNA strand.
 * Param start: The end of the gene in the DNA strand
 * Return value: None
*/
void Gene::set_end(const unsigned int end)
{
    this->_end = end;
}

/*
 * This function sets whatever the gene is on the complementary DNA strand.
 * Param start: A bool that indicates whatever the gene is on the complementary DNA strand
 * Return value: None
*/
void Gene::set_on_complementary_dna_strand(const bool on_complementary_dna_strand)
{
    this->_on_complementary_dna_strand = on_complementary_dna_strand;
}

/*
 * This function initializes the Nucleus class.
 * Param dna_sequence: The dna sequence stored in the nucleus of the cell.
 * Return value: None
*/
void Nucleus::init(const std::string dna_sequence)
{
    // Set the DNA strand as the given DNA seq
    this->_dna_strand = dna_sequence;

    // Create an empty string in the complementary to add to it the complementary for char in the original DNA seq
    this->_complementary_dna_strand = "";

    // For nucleotide in the DNA seq, create it's complementary
    for (char& nucleotide : this->_dna_strand)
    {
	// For each nucleotide in the seq add it's complementary to the complementary seq
	switch (nucleotide)
	{
	case 'G':
	    this->_complementary_dna_strand += "C";
	    break;

	case 'C':
	    this->_complementary_dna_strand += "G";
	    break;

	case 'T':
	    this->_complementary_dna_strand += "A";
	    break;

	case 'A':
	    this->_complementary_dna_strand += "T";
	    break;

	default:
	    // If invalid character found in the seq, print error and exit with error
	    cerr << "An invalid character found in the D.N.A seq!" << endl;
	    _exit(1); // Exit with error
	}
    }
}

/*
 * This function gets the RNA transcript of the gene from the DNA sequence
 * Param gene: The gene to get it's RNA transcript
 * Return value: The RNA transcript of the gene
*/
std::string Nucleus::get_RNA_transcript(const Gene& gene) const
{
    // Create an empty rna transcript
    std::string rna_transcript = "";

    // If the gene is in the complementary seq, take the RNA transcript from that string
    if (gene.is_on_complementary_dna_strand())
    {
	// Create a substring from the complementary seq with the gene's start and it's size (end - start + 1)
	rna_transcript = this->_complementary_dna_strand.substr(gene.get_start(), gene.get_end() - gene.get_start() + 1);
    }
    else {
	// Create a substring from the original seq with the gene's start and it's size (end - start + 1)
	rna_transcript = this->_dna_strand.substr(gene.get_start(), gene.get_end() - gene.get_start() + 1);
    }

    // Go through the nucleotides in the rna transcript seq and replace all T with U
    for (char& nucleotide : rna_transcript)
    {
	// Replace T with U
	if (nucleotide == 'T')
	{
	    nucleotide = 'U';
	}
    }

    return rna_transcript;
}

/*
 * This function reverses the DNA strand
 * Return value: The DNA strand reversed
*/
std::string Nucleus::get_reversed_DNA_strand() const
{
    // Create a copy of the original dna strand
    std::string reversed_dna_strand = this->_dna_strand;

    // Reverse the string using it's start and end characters
    std::reverse(reversed_dna_strand.begin(), reversed_dna_strand.end());

    return reversed_dna_strand;
}

/*
 * This function finds out the number of appearances of a codon in the DNA strand
 * Param codon: The codon to be searched in the DNA strand
 * Return value: The number of appearances of the codon in the DNA strand
*/
unsigned int Nucleus::get_num_of_codon_appearances(const std::string& codon) const
{
    unsigned int num_of_codon_appearances = 0;

    // Get the first appearance of the codon in the string
    int lastApperance = this->_dna_strand.find(codon, 0);

    // While we still find codons
    while (lastApperance != std::string::npos)
    {
	// Increase counter
	num_of_codon_appearances++;

	// Search for next substring (codon) in the original dna seq
	lastApperance = this->_dna_strand.find(codon, lastApperance);
    }

    return num_of_codon_appearances;
}

/*
 * This function gets the DNA strand field from the Nucleus
 * Return value: The DNA strand field
*/
std::string Nucleus::getDNAStrand() const
{
    return this->_dna_strand;
}
