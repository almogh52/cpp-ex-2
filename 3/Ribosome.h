#pragma once
#include <string>

#include "Protein.h"
#include "AminoAcid.h"

#define AMINO_ACID_SIZE 3

class Ribosome
{
public:
    Protein* create_protein(std::string &RNA_transcript) const;
};

