#pragma once

#include <string>

#include "Cell.h"

class Virus
{
private:
    std::string _RNA_sequence;

public:
    // Init function
    void init(const std::string RNA_sequence);

    // The infect function
    void infect_cell(Cell& cell) const;
};

