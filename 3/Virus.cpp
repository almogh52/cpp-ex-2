#include "Virus.h"

/*
 * This function initializes the Virus class.
 * Param RNA_sequence: The virus RNA sequence
 * Return value: None
*/
void Virus::init(const std::string RNA_sequence)
{
    this->_RNA_sequence = RNA_sequence;
}

/*
 * This function infects a cell using the virus.
 * Param cell: The reference to the cell that need to be infected
 * Return value: None
*/
void Virus::infect_cell(Cell & cell) const
{
    std::string dna_strand = "";
    std::string rna_strand = this->_RNA_sequence; // Create a copy of the original to no ruin it

    // Get the dna strand from the cell's nucleus
    dna_strand = cell.getNucleus().getDNAStrand();

    // Go through the rna strand and convert it into a dna strand
    for (char& nucleotide : rna_strand)
    {
	// If the nucleotide is U replace it by a T
	if (nucleotide == 'U')
	{
	    nucleotide = 'T';
	}
    }

    // Insert the virus dna strand (was rna strand) into the middle of the original dna strand
    dna_strand.insert(dna_strand.length() / 2, rna_strand);

    // Re-initialize the cell with the new dna strand that include the virus and the original glocus receptor gene
    cell.init(dna_strand, cell.getGlocusReceptorGene());
}
