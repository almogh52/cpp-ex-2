#pragma once
#include <iostream>
#include <string>

/*
* A class that represents a Gene (a D.N.A strand that is responsible for a certain attribute).
*/
class Gene
{
private:
    // Fields
    unsigned int _start;
    unsigned int _end;
    bool _on_complementary_dna_strand;

public:
    // Init function
    void init(const unsigned int start, const unsigned int end, const bool on_complementary_dna_strand);

    // Getters
    unsigned int get_start() const;
    unsigned int get_end() const;
    bool is_on_complementary_dna_strand() const;

    // Setters
    void set_start(const unsigned int start);
    void set_end(const unsigned int end);
    void set_on_complementary_dna_strand(const bool on_complementary_dna_strand);
    
};

/*
* A class the represent the Nucleus of the cell
*/
class Nucleus
{
private:
    std::string _dna_strand;
    std::string _complementary_dna_strand;

public:
    // Init method
    void init(const std::string dna_sequence);

    // Nucleus methods
    std::string get_RNA_transcript(const Gene& gene) const;
    std::string get_reversed_DNA_strand() const;
    unsigned int get_num_of_codon_appearances(const std::string& codon) const;

    // DNA strand getter (used in bonus)
    std::string getDNAStrand() const;
};